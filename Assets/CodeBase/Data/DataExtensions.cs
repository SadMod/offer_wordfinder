﻿using UnityEngine;

namespace CodeBase.Data
{
  public static class DataExtensions
  {
    /// <summary>
    /// Removes "(Clone)" from GameObject's name
    /// </summary>
    /// <param name="obj">GameObject that need to be renamed</param>
    public static void UnClone(this GameObject obj) =>
      obj.name = obj.name.Replace("(Clone)", string.Empty);

    /// <summary>
    /// Converts 2D string array to 2D char array
    /// </summary>
    /// <param name="source"><see cref="string[,]">2D string array</see></param>
    /// <returns><see cref="char[,]">2D char array</see></returns>
    public static char[,] To2DCharArray(this string[,] source)
    {
      int height = source.GetLength(0);
      int width = source.GetLength(1);

      char[,] result = new char[height, width];

      for (int i = 0; i < height; i++)
      for (int j = 0; j < width; j++)
        result[i, j] = char.Parse(source[i, j]);

      return result;
    }

    public static Transform GetTransform(this GameObject parent, string path) =>
      parent.transform.Find(path).transform;
  }
}