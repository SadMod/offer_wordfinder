﻿namespace CodeBase.UI.Components
{
  public interface IPuzzleGridItem
  {
    bool IsTileActive { get; set; }
    bool TileRevealed { get; set; }
    bool TileHasInput { get; }
    void Wave();
  }
}