﻿using System.Collections.Generic;
using UnityEngine;

namespace CodeBase.UI.Components
{
  public interface IWordSwipeArea
  {
    GameObject Item { get; }
    List<WordSwipeButton> Buttons { get; }
  }
}