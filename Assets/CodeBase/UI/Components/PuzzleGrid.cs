﻿using System.Collections.Generic;
using System.Linq;
using CodeBase.Infrastructure.PuzzleTools;
using DG.Tweening;
using UnityEngine;

namespace CodeBase.UI.Components
{
  public class PuzzleGrid : IPuzzleGrid
  {
    private GameObject Item { get; }
    public PuzzleInput Data { get; }
    public PuzzleGridItem[,] GridTilesData { get; }

    public PuzzleGrid(GameObject item, PuzzleInput data, List<(Transform, List<GameObject>)> structureData)
    {
      Item = item;
      Data = data;
      GridTilesData = PopulateGrid(structureData);
    }

    private PuzzleGridItem[,] PopulateGrid(List<(Transform, List<GameObject>)> structureData)
    {
      int height = Data.Height;
      int width = Data.Width;
      var gridVisuals = new PuzzleGridItem[height, width];

      for (int row = 0; row < height; row++)
      {
        var rowData = structureData[row];
        rowData.Item1.name = $"Row {row}";
        for (int column = 0; column < width; column++)
        {
          var item = rowData.Item2[column];

          char inputAtPosition = char.Parse(Data.GetAt(row, column));
          bool hasInput = char.IsLetter(inputAtPosition);
          string validInput = hasInput ? inputAtPosition.ToString() : string.Empty;

          item.name = $"[{row}, {column}] {validInput}";
          gridVisuals[row, column] = new PuzzleGridItem(item, validInput);
        }
      }

      return gridVisuals;
    }

    public void Reveal(PuzzleWordData wordData)
    {
      Sequence revealSequence = DOTween.Sequence();
      revealSequence.SetEase(Ease.OutSine);
      
      var revealDuration = 0.5f;

      var validTiles = wordData.Indexes
        .Select(letterData => GridTilesData[letterData.Row, letterData.Column])
        .Where(tile => tile.IsTileActive)
        .ToList();
      
      var revealCandidatesCount = validTiles.Count;

      for (var index = 0; index < validTiles.Count; index++)
      {
        var tile = validTiles[index];
        if (tile.TileRevealed) 
          revealSequence.InsertCallback(
            revealDuration / revealCandidatesCount * index, 
            tile.Wave);
        else 
          revealSequence.InsertCallback(
            revealDuration / revealCandidatesCount * index,
            () => tile.TileRevealed = true);
      }
    }
    public void WaveAllDown()
    {
      var sequence = DOTween.Sequence();
      sequence.SetEase(Ease.Linear);
      float animationDuration = 1f;

      for (int row = 0; row < Data.Height; row++)
      {        
        var pos = animationDuration / Data.Height * row;

        for (int column = 0; column < Data.Width; column++)
        {
          sequence.InsertCallback(pos, GridTilesData[row, column].Wave);
        }
      }
    }
  }
}