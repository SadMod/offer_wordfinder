﻿using System.Collections.Generic;
using CodeBase.Infrastructure.PuzzleTools;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CodeBase.UI.Components
{
  public interface IPuzzleGuessArea
  {
    Button DropGuess { get; }
    TMP_InputField GuessInputArea { get; }
    Button CommitGuess { get; }
    void DropAndResetGuess(List<WordSwipeButton> inputButtons);
    void CommitAndResetGuess(List<WordSwipeButton> inputButtons, string prompt);
  }
}