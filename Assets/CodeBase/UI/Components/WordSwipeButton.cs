﻿using CodeBase.Infrastructure.AssetManagement;
using Lean.Gui;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CodeBase.UI.Components
{
  public class WordSwipeButton
  {
    public GameObject Item { get; }
    public TMP_Text LetterLabel { get; }
    public LeanButton LetterInput { get; }

    public bool Interactable
    {
      get => LetterInput.interactable;
      set => LetterInput.interactable = value;
    }

    public WordSwipeButton(GameObject item)
    {
      Item = item;
      LetterInput = item.GetComponent<LeanButton>();
      LetterLabel = item.transform.Find(ScenePath.ControlsLabelRelativePath).GetComponent<TMP_Text>();
    }

    public void AddToGuessArea(TMP_InputField input, Button dropGuess, Button commitGuess)
    {
      input.text += LetterLabel.text;
      Interactable = false;
      dropGuess.interactable = true;
      commitGuess.interactable = input.text.Length > 1;
    }
  }
}