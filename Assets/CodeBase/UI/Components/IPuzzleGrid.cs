﻿using CodeBase.Infrastructure.PuzzleTools;
using UnityEngine;

namespace CodeBase.UI.Components
{
  public interface IPuzzleGrid
  {
    void Reveal(PuzzleWordData wordData);
    PuzzleInput Data { get; }
    PuzzleGridItem[,] GridTilesData { get; }
    void WaveAllDown();
  }
}