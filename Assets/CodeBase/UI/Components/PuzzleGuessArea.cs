﻿using System;
using System.Collections.Generic;
using CodeBase.Infrastructure.AssetManagement;
using CodeBase.Infrastructure.PuzzleTools;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CodeBase.UI.Components
{
  public class PuzzleGuessArea : IPuzzleGuessArea
  {
    private GameObject Item { get; }
    private IPuzzleMechanism PuzzleMechanism { get; }
    public Button DropGuess { get; }
    public TMP_InputField GuessInputArea { get; }
    public Button CommitGuess { get; }

    public PuzzleGuessArea(GameObject item, IPuzzleMechanism puzzleMechanism)
    {
      Item = item;
      PuzzleMechanism = puzzleMechanism;
      DropGuess = item.transform.Find(ScenePath.GuessAreaDropGuess).GetComponent<Button>();
      GuessInputArea = item.transform.Find(ScenePath.GuessAreaGuessInput).GetComponent<TMP_InputField>();
      CommitGuess = item.transform.Find(ScenePath.GuessAreaCommitGuess).GetComponent<Button>();
    }

    public void DropAndResetGuess(List<WordSwipeButton> inputButtons)
    {
      ResetInputButtons(inputButtons);
    }

    public void CommitAndResetGuess(List<WordSwipeButton> inputButtons, string prompt)
    {
      PuzzleMechanism.TryPrompt(prompt);
      ResetInputButtons(inputButtons);
    }

    private void ResetInputButtons(List<WordSwipeButton> inputButtons)
    {
      GuessInputArea.text = String.Empty;
      DropGuess.interactable = false;
      CommitGuess.interactable = false;
      inputButtons.ForEach(item => item.Interactable = true);
    }
  }
}