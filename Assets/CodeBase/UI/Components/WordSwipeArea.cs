﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace CodeBase.UI.Components
{
  public class WordSwipeArea : IWordSwipeArea
  {
    public GameObject Item { get; }
    public List<WordSwipeButton> Buttons { get; }

    public WordSwipeArea(GameObject item, List<SwipeButtonData> data)
    {
      Item = item;
      Buttons = PopulateSwipeArea(data);
    }

    private List<WordSwipeButton> PopulateSwipeArea(List<SwipeButtonData> data)
    {
      var buttonsData = new List<WordSwipeButton>();

      for (int i = 0; i < data.Count; i++)
      {
        var buttonItem = new WordSwipeButton(data[i].Item);
        buttonItem.LetterLabel.text = data[i].Letter.ToString();

        buttonsData.Add(buttonItem);
      }

      return buttonsData;
    }

    [Serializable]
    public class SwipeButtonData
    {
      public GameObject Item { get; }
      public char Letter { get; }

      public SwipeButtonData(GameObject item, char letter)
      {
        Item = item;
        Letter = letter;
      }
    }
  }
}