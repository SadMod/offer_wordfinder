﻿using CodeBase.Infrastructure.AssetManagement;
using Lean.Gui;
using Lean.Transition;
using Lean.Transition.Method;
using TMPro;
using UnityEngine;

namespace CodeBase.UI.Components
{
  public class PuzzleGridItem : IPuzzleGridItem
  {
    private GameObject Item { get; }
    private TMP_Text Label { get; }

    public bool IsTileActive
    {
      get => TileActivator.On;
      set => TileActivator.On = value;
    }

    public bool TileRevealed
    {
      get => Label.IsActive();
      set
      {
        if (value) AnimateReveal.BeginTransitions();
        else Label.gameObject.SetActive(false);
      }
    }

    public bool TileHasInput => Label.text.Length > 0;

    private LeanToggle TileActivator { get; }
    private readonly LeanEvent AnimateWave;
    private readonly LeanManualAnimation AnimateReveal;

    public PuzzleGridItem(GameObject item, string input)
    {
      Item = item;
      Label = item.transform.Find(ScenePath.GridItemLabel).GetComponent<TMP_Text>();
      Label.text = input;
      TileActivator = item.GetComponent<LeanToggle>();
      AnimateWave = item.transform.Find(ScenePath.GridItemAnimatorWave).GetComponent<LeanEvent>();
      AnimateReveal = item.transform.Find(ScenePath.GridItemAnimatorReveal).GetComponent<LeanManualAnimation>();
    }

    public void Wave() => AnimateWave.BeginThisTransition();
  }
}