﻿using CodeBase.Infrastructure.AssetManagement;
using CodeBase.Infrastructure.Factory;
using CodeBase.Infrastructure.PuzzleTools;
using CodeBase.Infrastructure.PuzzleTools.Guess;
using CodeBase.Infrastructure.PuzzleTools.Solver;
using CodeBase.Services;

namespace CodeBase.Infrastructure.States
{
  public class BootstrapState : IState
  {
    private const string Initial = "Initial";
    private const string MainSceneName = "Main";

    private readonly GameStateMachine _stateMachine;
    private readonly SceneLoader _sceneLoader;
    private readonly AllServices _services;

    public BootstrapState(GameStateMachine stateMachine, SceneLoader sceneLoader, AllServices services)
    {
      _stateMachine = stateMachine;
      _sceneLoader = sceneLoader;
      _services = services;
      RegisterServices();
    }

    public void Enter()
    {
      _sceneLoader.Load(Initial, EnterLoadLevel);
    }

    public void Exit()
    {
    }

    private void EnterLoadLevel() =>
      _stateMachine.Enter<LoadLevelState, string>(MainSceneName);

    private void RegisterServices()
    {
      _services.RegisterSingle<IAssets>(new AssetProvider());
      _services.RegisterSingle<IPuzzleSolver>(new Solver());
      _services.RegisterSingle<IPuzzleGuesser<PuzzleWordData>>(new Guesser());
      //_services.RegisterSingle<IPuzzleMechanism>();
      _services.RegisterSingle<IGameFactory>(new GameFactory(
        _services.Single<IAssets>(), 
        _services.Single<IPuzzleSolver>(),
        _services.Single<IPuzzleGuesser<PuzzleWordData>>()));
    }
  }
}