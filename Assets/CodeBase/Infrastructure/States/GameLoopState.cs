﻿using CodeBase.Infrastructure.Factory;
using CodeBase.Services;
using DG.Tweening;
using System.Linq;
using CodeBase.Infrastructure.AssetManagement;
using CodeBase.UI.Components;
using Michsky.MUIP;

namespace CodeBase.Infrastructure.States
{
  public class GameLoopState : IState
  {
    private readonly GameStateMachine _gameStateMachine;
    private readonly IGameFactory gameFactory;

    public GameLoopState(GameStateMachine gameStateMachine)
    {
      _gameStateMachine = gameStateMachine;
      gameFactory = AllServices.Container.Single<IGameFactory>();
    }

    public void Enter()
    {
      ScheduleOrAnimateGridActivation();
      ScheduleOrAnimateWordSwipeArea();
      ScheduleOrSetCallbacksForWordSwipeArea();
    }

    private void ScheduleOrAnimateGridActivation()
    {
      if (gameFactory.GridInstance is not null) AnimateGridActivation();
      else gameFactory.GridCreated += AnimateGridActivation;
    }

    private void ScheduleOrAnimateWordSwipeArea()
    {
      if (gameFactory.WordSwipeAreaInstance is not null) AnimateWordSwipeAreaActivation();
      else gameFactory.WordSwipeAreaCreated += AnimateWordSwipeAreaActivation;
    }

    private void ScheduleOrSetCallbacksForWordSwipeArea()
    {
      if (gameFactory.WordSwipeAreaInstance is not null) SetCallbacksForWordSwipeArea();
      else gameFactory.WordSwipeAreaCreated += SetCallbacksForWordSwipeArea;
    }

    public void Exit()
    {
      gameFactory.GridCreated -= AnimateGridActivation;
      gameFactory.WordSwipeAreaCreated -= AnimateWordSwipeAreaActivation;
      gameFactory.WordSwipeAreaCreated -= SetCallbacksForWordSwipeArea;
    }

    private void AnimateGridActivation()
    {
      var gridData = gameFactory.GridInstance.GridTilesData;

      Sequence activationSequence = DOTween.Sequence();

      float activationDelay = 2.5f;
      float activationDuration = 2;

      activationSequence
        .SetEase(Ease.InOutSine) //InOutCubic
        .SetDelay(activationDelay);

      var scheduledItems =
        (from PuzzleGridItem gridItem in gridData where gridItem.TileHasInput select gridItem).ToList();

      scheduledItems.ForEach(scheduledItem => activationSequence.InsertCallback(
        activationDuration / scheduledItems.Count * scheduledItems.IndexOf(scheduledItem),
        () => scheduledItem.IsTileActive = true));
    }

    private void AnimateWordSwipeAreaActivation()
    {
      var layoutGroup =
        gameFactory.WordSwipeAreaInstance.Item.transform
          .Find(ScenePath.ControlButtonsRelativePath)
          .GetComponent<RadialLayoutGroup>();
      layoutGroup.CalculateLayoutInputHorizontal();

      // TODO: Find a way to reliably calculate field for radial layout. (Math is tough т.т)
      //DOTween.To(value => layoutGroup.angleRange = ) 
      //x1 = 180, x2 = 240, x3 = 270, x4 = 290, x5 = 300, x6 = 310, X , x8 = 315
    }

    private void SetCallbacksForWordSwipeArea()
    {
      var inputButtons = gameFactory.WordSwipeAreaInstance.Buttons;
      var guessArea = gameFactory.PuzzleGuessArea;
      var dropGuess = guessArea.DropGuess;
      var inputField = guessArea.GuessInputArea;
      var commitGuess = guessArea.CommitGuess;

      inputButtons.ForEach(button =>
        button.LetterInput.OnClick.AddListener(() =>
          button.AddToGuessArea(inputField, dropGuess, commitGuess)));
      dropGuess.onClick.AddListener(() =>
        guessArea.DropAndResetGuess(inputButtons));
      commitGuess.onClick.AddListener(() =>
        guessArea.CommitAndResetGuess(inputButtons, inputField.text));
    }
  }
}