﻿namespace CodeBase.Infrastructure.AssetManagement
{
  public static class ScenePath
  {
    public const string GridRelativePath = "Content/Grid area";
    public const string ControlsRelativePath = "Content/Controls area";
    public const string ControlsLabelRelativePath = "Label";
    public const string GridItemLabel = "Item/Label";
    public const string GridItemAnimatorWave = "[Transitions]/Wave";
    public const string GridItemAnimatorReveal = "[Transitions]/Reveal";
    public const string ControlButtonsRelativePath = "Content";
    public const string GuessArea = "Content/Guess area";
    public const string GuessAreaDropGuess = "[Button] Drop guess";
    public const string GuessAreaGuessInput = "[InputField] Guess input area";
    public const string GuessAreaCommitGuess = "[Button] Commit guess";
  }
}