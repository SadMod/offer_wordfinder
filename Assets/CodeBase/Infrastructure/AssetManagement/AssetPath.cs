﻿namespace CodeBase.Infrastructure.AssetManagement
{
  public static class AssetPath
  {
    public const string PuzzleInputJson = "Settings/PuzzleInput";
    public const string PuzzleBehaviorJson = "Settings/PuzzleBehavior";
    
    public const string HudPath = "Hud/Hud";

    public const string GridPath = "Hud/Word Grid/1. Puzzle Grid";
    public const string GridRowPath = "Hud/Word Grid/1.X. GridRowPrefab";
    public const string GridTilePath = "Hud/Word Grid/1.X.X. GridTilePrefab";

    public const string WordSwipeArea = "Hud/Word Swipe Input/Swipe area";
    public const string WordSwipeButton = "Hud/Word Swipe Input/Swipe button";
    
    public const string SoundFailure = "Sounds/8Bit_Fail";
    public const string SoundFailure_StevenHe = "Sounds/Steven He - (My Son is a) Failure (Cut)";
  }
}