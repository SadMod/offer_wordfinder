﻿using System.Threading.Tasks;
using CodeBase.Services;
using UnityEngine;
using UnityEngine.Events;

namespace CodeBase.Infrastructure.AssetManagement
{
  public interface IAssets : IService
  {
    GameObject Instantiate(string path);
    GameObject Instantiate(string path, Vector3 at);
    GameObject Instantiate(string path, Transform parent);
    T LoadFromResources<T>(string path) where T : Object;
    T ParseFromJson<T>(string obj);
    T LoadJsonFromResources<T>(string path);
    //T LoadJsonFromStreamingAssets<T>(string path);
    //T LoadFromStreamingAssets<T>(string path) where T : Object;
  }
}