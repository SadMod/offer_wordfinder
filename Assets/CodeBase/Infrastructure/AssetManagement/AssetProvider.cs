﻿using Newtonsoft.Json;
using UnityEngine;

namespace CodeBase.Infrastructure.AssetManagement
{
  public class AssetProvider : IAssets
  {
    public GameObject Instantiate(string path)
    {
      var prefab = Resources.Load<GameObject>(path);
      return Object.Instantiate(prefab);
    }

    public GameObject Instantiate(string path, Vector3 at)
    {
      var prefab = Resources.Load<GameObject>(path);
      return Object.Instantiate(prefab, at, Quaternion.identity);
    }

    public GameObject Instantiate(string path, Transform parent)
    {
      var prefab = Resources.Load<GameObject>(path);
      return Object.Instantiate(prefab, parent);
    }

    public T LoadFromResources<T>(string path) where T : Object =>
      Resources.Load<T>(path);

    // TODO: will need to figure out how to load assets async without Coroutines, or adapt architecture for Tasks
    // public T LoadFromStreamingAssets<T>(string path) where T : Object
    // {
    //   path = Path.Combine(Application.streamingAssetsPath, path);
    // }

    public T ParseFromJson<T>(string obj) =>
      JsonConvert.DeserializeObject<T>(obj);

    public T LoadJsonFromResources<T>(string path) =>
      ParseFromJson<T>(LoadFromResources<TextAsset>(path).text);

    // public T LoadJsonFromStreamingAssets<T>(string path)
    // {
    //   var asset = LoadFromStreamingAssets<TextAsset>(path);
    //   var rawJson = asset.text;
    //   return ParseFromJson<T>(rawJson);
    // }
  }
}