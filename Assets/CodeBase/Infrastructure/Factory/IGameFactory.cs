﻿using CodeBase.Services;
using System;
using CodeBase.UI.Components;
using UnityEngine;

namespace CodeBase.Infrastructure.Factory
{
  public interface IGameFactory : IService
  {
    GameObject HudInstance { get; }
    IWordSwipeArea WordSwipeAreaInstance { get; }
    IPuzzleGrid GridInstance { get; }
    IPuzzleGuessArea PuzzleGuessArea { get; }
    event Action HudCreated;
    event Action GridCreated;
    event Action WordSwipeAreaCreated;
    GameObject CreateHud();
    IPuzzleGrid CreateGameGrid();
    IWordSwipeArea CreateWordSwipeArea();
    void PlaySoundOnce(string alias);
  }
}