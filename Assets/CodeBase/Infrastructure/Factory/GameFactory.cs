﻿using System;
using System.Collections.Generic;
using System.Linq;
using CodeBase.Data;
using CodeBase.Infrastructure.AssetManagement;
using CodeBase.Infrastructure.PuzzleTools;
using CodeBase.Infrastructure.PuzzleTools.Guess;
using CodeBase.Infrastructure.PuzzleTools.Solver;
using CodeBase.UI.Components;
using DG.Tweening;
using UnityEngine;

namespace CodeBase.Infrastructure.Factory
{
  public class GameFactory : IGameFactory
  {
    public GameObject HudInstance { get; private set; }
    public IPuzzleGrid GridInstance { get; private set; }
    public IWordSwipeArea WordSwipeAreaInstance { get; private set; }
    public IPuzzleGuessArea PuzzleGuessArea { get; private set; }
    private IPuzzleMechanism PuzzleController { get; set; }

    public event Action HudCreated;
    public event Action GridCreated;
    public event Action WordSwipeAreaCreated;

    private readonly IAssets assets;

    public GameFactory(IAssets assets, IPuzzleSolver solver, IPuzzleGuesser<PuzzleWordData> guesser)
    {
      this.assets = assets;
      var config = assets.LoadJsonFromResources<PuzzleBehavior>(AssetPath.PuzzleBehaviorJson);
      GridCreated += () => PuzzleController = new PuzzleMechanism(this, config, GridInstance, solver, guesser);
    }

    public GameObject CreateHud()
    {
      HudInstance = InstantiateRegistered(AssetPath.HudPath);
      HudInstance.UnClone();
      GridCreated += () => PuzzleGuessArea = new PuzzleGuessArea(HudInstance.transform.Find(ScenePath.GuessArea).gameObject, PuzzleController);
      HudCreated?.Invoke();
      return HudInstance;
    }

    public IPuzzleGrid CreateGameGrid()
    {
      var gridInstance = InstantiateRegistered(AssetPath.GridPath,
        HudInstance.GetTransform(ScenePath.GridRelativePath));
      var data = assets.LoadJsonFromResources<PuzzleInput>(AssetPath.PuzzleInputJson);

      var rows = InstantiateGridRows(data.Height, gridInstance.transform);
      List<Transform> rowTransforms = rows.Select(row => row.transform).ToList();

      var structureData = 
        rowTransforms.ConvertAll(rowTransform => (rowTransform, InstantiateGridTiles(data.Width, rowTransform)));

      gridInstance.UnClone();
      
      GridInstance = new PuzzleGrid(gridInstance, data, structureData);

      GridCreated?.Invoke();
      return GridInstance;

      List<GameObject> InstantiateGridRows(int height, Transform parent)
      {
        List<GameObject> rows = new();
        for (int row = 0; row < height; row++) rows.Add(InstantiateRegistered(AssetPath.GridRowPath, parent));
        return rows;
      }

      List<GameObject> InstantiateGridTiles(int width, Transform parentRow)
      {
        List<GameObject> tiles = new();
        for (int column = 0; column < width; column++)
        {
          tiles.Add(InstantiateRegistered(AssetPath.GridTilePath, parentRow));
        }

        return tiles;
      }
    }

    public IWordSwipeArea CreateWordSwipeArea()
    {
      var swipeAreaInstance =
        InstantiateRegistered(AssetPath.WordSwipeArea,
          HudInstance.transform.Find(ScenePath.ControlsRelativePath).transform);

      var letters = PuzzleController.GetLetterOptions();

      List<WordSwipeArea.SwipeButtonData> swipeAreaData = SwipeAreaData(letters, swipeAreaInstance);

      swipeAreaInstance.UnClone();
      WordSwipeAreaInstance = new WordSwipeArea(swipeAreaInstance, swipeAreaData);

      WordSwipeAreaCreated?.Invoke();
      return WordSwipeAreaInstance;

      List<WordSwipeArea.SwipeButtonData> SwipeAreaData(List<char> chars, GameObject gameObject) =>
        (from t in chars
          let button = InstantiateRegistered(
            AssetPath.WordSwipeButton, gameObject.transform.Find(ScenePath.ControlButtonsRelativePath))
          select new WordSwipeArea.SwipeButtonData(button, t)).ToList();
    }

    public void PlaySoundOnce(string alias)
    {
      var speaker = new GameObject().AddComponent<AudioSource>();
      var clip = LoadFromAlias(alias);

      speaker.PlayOneShot(clip);
      GameObject.Destroy(speaker.gameObject, clip.length);
      
      AudioClip LoadFromAlias(string alias)
      {
        return alias switch
        {
          "PlaySoundFailure" => 
            assets.LoadFromResources<AudioClip>(AssetPath.SoundFailure),
          "PlaySoundFailure_StevenHe" => 
            assets.LoadFromResources<AudioClip>(AssetPath.SoundFailure_StevenHe),
          _ => throw new ArgumentOutOfRangeException()
        };
      }
    }

    private GameObject InstantiateRegistered(string prefabPath, Vector3 at)
    {
      GameObject gameObject = assets.Instantiate(prefabPath, at);
      return gameObject;
    }

    private GameObject InstantiateRegistered(string prefabPath)
    {
      GameObject gameObject = assets.Instantiate(prefabPath);
      return gameObject;
    }

    private GameObject InstantiateRegistered(string prefabPath, Transform parent)
    {
      GameObject gameObject = assets.Instantiate(prefabPath, parent);
      return gameObject;
    }
  }
}