﻿using Newtonsoft.Json;

namespace CodeBase.Infrastructure.PuzzleTools
{
  [JsonObject]
  public class PuzzleBehavior
  {
    [JsonProperty] 
    public OnIncorrect onIncorrect;

    [JsonObject]
    public class OnIncorrect
    {
      public enum OnIcorrectActions
      {
        AnimateWave,
        PlaySoundFailure,
        PlaySoundFailure_StevenHe
      }

      [JsonProperty]
      public OnIcorrectActions doAction;
    }
  }
}