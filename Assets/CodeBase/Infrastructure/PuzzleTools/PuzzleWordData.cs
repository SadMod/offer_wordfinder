﻿using System;
using System.Collections.Generic;

namespace CodeBase.Infrastructure.PuzzleTools
{
  [Serializable]
  public class PuzzleWordData
  {
    public string Word { get; }
    public List<Coordinate> Indexes { get; }

    public PuzzleWordData(string word, List<Coordinate> indexes)
    {
      Word = word;
      Indexes = indexes;
    }

    [Serializable]
    public class Coordinate
    {
      public int Row { get; }
      public int Column { get; }

      public Coordinate(int row, int column)
      {
        Row = row;
        Column = column;
      }
    }
  }
}