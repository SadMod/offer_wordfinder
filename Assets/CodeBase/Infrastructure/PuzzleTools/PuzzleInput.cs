﻿using System;
using Newtonsoft.Json;

namespace CodeBase.Infrastructure.PuzzleTools
{
  [JsonObject]
  public class PuzzleInput
  {
    [JsonProperty] public string[,] content;

    public int Width => content.GetLength(0);
    public int Height => content.GetLength(1);

    public string GetAt(int Row, int Column)
    {
      if (content is null) throw new ArgumentNullException();
      if (Width < Column || Height < Row) throw new IndexOutOfRangeException();
      return content[Row, Column];
    }
  }
}