﻿using System;
using System.Collections.Generic;
using System.Linq;
using CodeBase.Data;
using CodeBase.Infrastructure.Factory;
using CodeBase.Infrastructure.PuzzleTools.Guess;
using CodeBase.Infrastructure.PuzzleTools.Solver;
using CodeBase.UI.Components;

namespace CodeBase.Infrastructure.PuzzleTools
{
  public class PuzzleMechanism : IPuzzleMechanism
  {
    private IPuzzleSolver Solver { get; }
    private IPuzzleGuesser<PuzzleWordData> Guesser { get; }
    private IPuzzleGrid Grid { get; }
    private IGameFactory GameFactory { get; }
    private PuzzleBehavior Config;

    private List<PuzzleWordData> SolverResult
    {
      get
      {
        if (solverResult is null) Solver.TrySolve(Grid.Data.content.To2DCharArray(), out solverResult);
        return solverResult;
      }
    }
    private List<PuzzleWordData> solverResult;

    public PuzzleMechanism(IGameFactory gameFactory, PuzzleBehavior config, IPuzzleGrid grid, IPuzzleSolver solver,
      IPuzzleGuesser<PuzzleWordData> guesser)
    {
      GameFactory = gameFactory;
      Config = config;
      Grid = grid;
      Solver = solver;
      Guesser = guesser;
    }

    public List<char> GetLetterOptions() =>
      SolverResult
        .Aggregate("", (max, cur) => max.Length > cur.Word.Length ? max : cur.Word)
        .OrderBy(c => c)
        .ToList();

    
    public void TryPrompt(string input)
    {
      var isCorrect = Guesser.TryGuess(input, SolverResult, out var result);
      if (isCorrect)
      {
        Grid.Reveal(result);
      }
      else
      {
        switch (Config.onIncorrect.doAction)
        {
          case PuzzleBehavior.OnIncorrect.OnIcorrectActions.AnimateWave:
            Grid.WaveAllDown();
            break;
          case PuzzleBehavior.OnIncorrect.OnIcorrectActions.PlaySoundFailure:
          case PuzzleBehavior.OnIncorrect.OnIcorrectActions.PlaySoundFailure_StevenHe:
          GameFactory.PlaySoundOnce(Enum.GetName(typeof(PuzzleBehavior.OnIncorrect.OnIcorrectActions), Config.onIncorrect.doAction));
            break;
          default:
            throw new ArgumentOutOfRangeException();
        }
      }
    }
  }
}