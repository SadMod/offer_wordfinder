﻿using System.Collections.Generic;
using CodeBase.Infrastructure.PuzzleTools.Guess;
using CodeBase.Infrastructure.PuzzleTools.Solver;
using CodeBase.Services;
using CodeBase.UI.Components;

namespace CodeBase.Infrastructure.PuzzleTools
{
  public interface IPuzzleMechanism : IService
  {
    List<char> GetLetterOptions();
    void TryPrompt(string input);
  }
}