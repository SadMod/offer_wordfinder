﻿using System.Collections.Generic;
using CodeBase.Services;

namespace CodeBase.Infrastructure.PuzzleTools.Guess
{
  public interface IPuzzleGuesser<T> : IService
  {
    bool TryGuess(string prompt, List<T> input, out T result);
  }
}