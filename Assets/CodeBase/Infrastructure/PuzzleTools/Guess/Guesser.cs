﻿using System.Collections.Generic;
using System.Linq;

namespace CodeBase.Infrastructure.PuzzleTools.Guess
{
  public class Guesser : IPuzzleGuesser<PuzzleWordData>
  {
    public bool TryGuess(string prompt, List<PuzzleWordData> input, out PuzzleWordData result) =>
      (result = input.FirstOrDefault(data => data.Word == prompt)) is not null;
  }
}