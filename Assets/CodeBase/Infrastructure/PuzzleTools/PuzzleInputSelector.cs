﻿using System;
using System.IO;
using Sirenix.OdinInspector;
using UnityEngine;

namespace CodeBase.Infrastructure.PuzzleTools
{
  [Serializable, Obsolete]
  public class PuzzleInputSelector
  {
    public enum PuzzleInputSelectorModes
    {
      Asset,
      Path
    }

    public PuzzleInputSelectorModes Mode = PuzzleInputSelectorModes.Asset;

    [SerializeField, ShowIf(nameof(Mode), PuzzleInputSelectorModes.Asset), AssetList]
    private TextAsset inputFileAsset;

    [ShowIf(nameof(Mode), PuzzleInputSelectorModes.Path), FilePath]
    public string inputFilePath;

    public string ReadData()
    {
      return Mode switch
      {
        PuzzleInputSelectorModes.Asset => inputFileAsset.text,
        PuzzleInputSelectorModes.Path => File.ReadAllText(inputFilePath),
        _ => throw new ArgumentException()
      };
    }
  }
}