﻿using System.Collections.Generic;
using CodeBase.Services;

namespace CodeBase.Infrastructure.PuzzleTools.Solver
{
  public interface IPuzzleSolver : IService
  {
    bool TrySolve(char[,] input, out List<PuzzleWordData> result);
  }
}