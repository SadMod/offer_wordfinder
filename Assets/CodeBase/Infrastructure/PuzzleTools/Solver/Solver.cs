﻿using System.Collections.Generic;
using System.Linq;

namespace CodeBase.Infrastructure.PuzzleTools.Solver
{
  public class Solver : IPuzzleSolver
  {
    public bool TrySolve(char[,] input, out List<PuzzleWordData> result) => _TrySolve(input, out result);

    public static bool _TrySolve(char[,] input, out List<PuzzleWordData> result)
    {
      var width = input.GetLength(0);
      var height = input.GetLength(1);

      var rows = FindRows(input, width, height);
      var columns = FindColumns(input, width, height);

      result = rows.Concat(columns).ToList();

      return result.Count > 0;
    }

    private static List<PuzzleWordData> FindRows(char[,] input, int width, int height)
    {
      List<PuzzleWordData> result = new();

      for (int row = height - 1; row >= 0; row--)
      {
        Queue<char> wordCandidate = new();
        Queue<PuzzleWordData.Coordinate> wordIndexes = new();
        for (int column = width - 1; column >= 0; column--)
        {
          var isValid = Check(input, column, row, wordCandidate, wordIndexes);
          if (isValid && column > 0) continue;
          if (wordCandidate.Count > 1)
          {
            result.Add(new PuzzleWordData(string.Concat(wordCandidate.Reverse()), wordIndexes.Reverse().ToList()));
            wordCandidate.Clear();
            wordIndexes.Clear();
          }
          else
          {
            wordCandidate.Clear();
            wordIndexes.Clear();
          }
        }
      }

      return result;
    }

    private static List<PuzzleWordData> FindColumns(char[,] input, int width, int height)
    {
      List<PuzzleWordData> result = new();

      for (int column = width - 1; column >= 0; column--)
      {
        Queue<char> wordCandidate = new();
        Queue<PuzzleWordData.Coordinate> wordIndexes = new();
        for (int row = height - 1; row >= 0; row--)
        {
          var isValid = Check(input, column, row, wordCandidate, wordIndexes);
          if (isValid && row > 0) continue;
          if (wordCandidate.Count > 1)
          {
            result.Add(new PuzzleWordData(string.Concat(wordCandidate.Reverse()), wordIndexes.Reverse().ToList()));
            wordCandidate.Clear();
            wordIndexes.Clear();
          }
          else
          {
            wordCandidate.Clear();
            wordIndexes.Clear();
          }
        }
      }

      return result;
    }

    private static bool Check(char[,] input, int column, int row, Queue<char> wordCandidate,
      Queue<PuzzleWordData.Coordinate> wordIndexes)
    {
      char symbol = input[row, column];
      bool isValid = char.IsLetter(symbol);

      if (isValid)
      {
        wordCandidate.Enqueue(symbol);
        wordIndexes.Enqueue(new(row, column));
      }

      return isValid;
    }
  }
}