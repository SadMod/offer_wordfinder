using System.Collections.Generic;
using NUnit.Framework;
using System.Linq;

namespace CodeBase.Infrastructure.PuzzleTools.Solver.UnitTest.PuzzleSolver_UnitTests_Editor
{
  public class SolverTest
  {
    [Test]
    public void FindWordsHorizontally()
    {
      char[,] input =
      {
        { ' ', ' ', 'c', ' ', 'a', ' ', ' ', ' ', ' ', ' ' },
        { ' ', ' ', 'a', 'r', 'c', 't', 'i', 'c', ' ', ' ' },
        { ' ', ' ', 'p', ' ', 't', ' ', ' ', 'o', ' ', ' ' },
        { ' ', ' ', 'a', ' ', 'o', ' ', ' ', 'p', 'o', 't' },
        { ' ', ' ', 'c', 'a', 'r', ' ', ' ', ' ', ' ', ' ' },
        { ' ', ' ', 'i', ' ', ' ', ' ', ' ', ' ', ' ', ' ' },
        { ' ', ' ', 't', 'a', 'p', 'i', 'o', 'c', 'a', ' ' },
        { 'c', ' ', 'o', ' ', 'i', ' ', ' ', ' ', 'c', ' ' },
        { 'a', 'i', 'r', ' ', 't', 'o', 'r', ' ', 't', ' ' },
        { 't', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ' }
      };

      Dictionary<string, List<(int, int)>> expectedResults = new()
      {
        { "arctic", new() { (1, 2), (1, 3), (1, 4), (1, 5), (1, 6), (1, 7) } },
        { "pot", new() { (3, 7), (3, 8), (3, 9) } },
        { "car", new() { (4, 2), (4, 3), (4, 4) } },
        { "tapioca", new() { (6, 2), (6, 3), (6, 4), (6, 5), (6, 6), (6, 7), (6, 8) } },
        { "air", new() { (8, 0), (8, 1), (8, 2) } },
        { "tor", new() { (8, 4), (8, 5), (8, 6) } },
      };

      MatchResults(input, expectedResults);
    }

    [Test]
    public void FindWordsVertically()
    {
      char[,] input =
      {
        { ' ', ' ', 'c', ' ', 'a', ' ', ' ', ' ', ' ', ' ' },
        { ' ', ' ', 'a', 'r', 'c', 't', 'i', 'c', ' ', ' ' },
        { ' ', ' ', 'p', ' ', 't', ' ', ' ', 'o', ' ', ' ' },
        { ' ', ' ', 'a', ' ', 'o', ' ', ' ', 'p', 'o', 't' },
        { ' ', ' ', 'c', 'a', 'r', ' ', ' ', ' ', ' ', ' ' },
        { ' ', ' ', 'i', ' ', ' ', ' ', ' ', ' ', ' ', ' ' },
        { ' ', ' ', 't', 'a', 'p', 'i', 'o', 'c', 'a', ' ' },
        { 'c', ' ', 'o', ' ', 'i', ' ', ' ', ' ', 'c', ' ' },
        { 'a', 'i', 'r', ' ', 't', 'o', 'r', ' ', 't', ' ' },
        { 't', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ' }
      };

      Dictionary<string, List<(int, int)>> expectedResults = new()
      {
        { "cat", new() { (7, 0), (8, 0), (9, 0) } },
        { "capacitor", new() { (0, 2), (1, 2), (2, 2), (3, 2), (4, 2), (5, 2), (6, 2), (7, 2), (8, 2), } },
        { "actor", new() { (0, 4), (1, 4), (2, 4), (3, 4), (4, 4) } },
        { "pit", new() { (6, 4), (7, 4), (8, 4) } },
        { "cop", new() { (1, 7), (2, 7), (3, 7) } },
        { "act", new() { (6, 8), (7, 8), (8, 8) } },
      };

      MatchResults(input, expectedResults);
    }

    private static void MatchResults(char[,] input, Dictionary<string, List<(int, int)>> expectedResults)
    {
      var isSolved = Solver._TrySolve(input, out var result);

      List<string> wordsAndCoords = result.Select(wordData =>
        $@"""{wordData.Word}"": ({string.Join(", ", wordData.Indexes.ConvertAll(coordinate =>
          $"[{coordinate.Row}, {coordinate.Column}]"))})").ToList();

      TestContext.Write(string.Join(";\n", wordsAndCoords));

      Assert.IsTrue(isSolved);

      foreach (var expectedResult in expectedResults)
      {
        Assert.IsTrue(result.Any(pair =>
          pair.Word == expectedResult.Key
          && pair.Indexes.TrueForAll(coordinate =>
            expectedResult.Value.Contains((coordinate.Row, coordinate.Column)))));
      }
    }
  }
}