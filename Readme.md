# Offer Test Project

## Project Goal

- To have
  - [✅] Word searching algorithm
  - [✅] Reveal animation (letter by letter)
  - [✅] Conditional action on player mistake: Wave or Sound playing (Defined by Behavior Config)
  - [✅] In-Editor layout Preview
- Input Must be / Must have
  - [✅] Json config with grid of 10x10 ("Assets/Resources/Settings/PuzzleInput.json")
  - [✅] Grid cells, that either empty, or contains letter
  - [✅] Grid cells placed in the way, that allows to build a word. Horizontally, left to right and/or vertically from top to the bottom
  - [✅] Grid filled with any words candidate (Me) wishes.
  - [✅] Json config with behavior description, that defines what need to happen if player made a mistake ("Assets/Resources/Settings/PuzzleBehavior.json")
- UI Must have
  - [✅] 10x10 Grid, Input field, "OK" button
- Important notes
  - [✅] Application must have entry point
  - [❌] Application must use Zenject (Failed, due to current lack of knowledge)
  - [❌] Application must use C# Tasks (Failed, no case for usage found during development)
  - [✅] Application must have minimum to none MonoBehaviour's

## Tools

- Unity 2022.2.10f1
  - DoTween 1.2.705
  - Zenject 9.2.0
  - LeanGUI 2.0.1
  - ModernUI Pack 5.5.14
  - Odin Inspector 3.1.12.2
  - TMPro 3.0.6
- Rider 2022.1